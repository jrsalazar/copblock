//
//  CopBlockTests.m
//  CopBlockTests
//
//  Created by Charles Trankler on 10/16/14.
//  Copyright (c) 2014 Coolio Software. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface CopBlockTests : XCTestCase

@end

@implementation CopBlockTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
