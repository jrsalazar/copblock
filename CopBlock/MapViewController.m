//
//  MapViewController.m
//  CopBlock
//
//  Created by Charles Trankler on 10/16/14.
//  Copyright (c) 2014 Coolio Software. All rights reserved.
//

#import "MapViewController.h"
#import "AppDelegate.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface MapViewController ()

@property SystemSoundID soundID;

@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([CLLocationManager locationServicesEnabled]) {
        _locationManager = [[ CLLocationManager alloc ] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        [_locationManager startUpdatingLocation];
        //self.locationManager.distanceFilter = 75;
        //self.locationManager.desiredAccuracy = 20;
    }
    
    self.map.delegate = self;
    [self.map setShowsUserLocation:YES];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:[appDelegate currentLocation].coordinate radius:500];
    
    appDelegate.inCopZone = false;
    appDelegate.copRadius = 500; // 500 meters
    appDelegate.newestCopId = 0;
    appDelegate.cops = [[NSMutableArray alloc] init];
    
    [_map addOverlay:circle];
    [self loadNewCops];
    
    // update cops every 30 seconds
    [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(loadNewCops) userInfo:nil repeats:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if ([defaults integerForKey:@"copDays"] == 0) {
        appDelegate.copDays = 7;
    }
    else
        appDelegate.copDays = [defaults integerForKey:@"copDays"];
    
    [self loadNewCops];
}

-(void)drawCops:(NSArray *) cops {
    // display cops on map
    for (int i = 0; i < [cops count]; i++) {
        Cop *cop = cops[i];
        MKPointAnnotation *point = [[MKPointAnnotation alloc]init];
        
        point.coordinate = CLLocationCoordinate2DMake([cop.latitude doubleValue], [cop.longitude doubleValue]);
        point.title = @"cop";
        [_map addAnnotation:point];
    }
}
-(void)clearCops {
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate.cops removeAllObjects];
    [_map removeAnnotations:_map.annotations];
    appDelegate.newestCopId = 0;
}

- (void)loadNewCops {
    [self clearCops];
    NSMutableArray *newCops = [[NSMutableArray alloc] init];
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    
    NSString *url = @"http://wise-chalice-771.appspot.com/getNewCops.php?id=";
    url = [url stringByAppendingString:[NSString stringWithFormat:@"%d", app.newestCopId]];
    url = [url stringByAppendingString:@"&days="];
    url = [url stringByAppendingString:[NSString stringWithFormat:@"%d", app.copDays]];
    
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    NSData *resp = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:resp options:0 error:nil];
    NSArray *cops = [json objectForKey:@"cops"];
    
    for (int i = 0; i < [cops count]; i++) {
        Cop *cop = [[Cop alloc] init];
        NSDictionary *copJSON = cops[i];
        
        cop.copId = @([[copJSON objectForKey:@"cop_id"] intValue]);
        cop.latitude = @([[copJSON objectForKey:@"lat"] doubleValue]);
        cop.longitude = @([[copJSON objectForKey:@"long"] doubleValue]);
        cop.createDate = [copJSON objectForKey:@"create_date"];
        
        [newCops addObject:cop];
        app.newestCopId = [cop.copId integerValue];
        [app.cops addObject:cop];
    }
    
    [self drawCops:newCops];
}

/*
 - (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
 if ([annotation isKindOfClass:[CopAnnotation class]]) {
 CopAnnotation *cop = (CopAnnotation *) annotation;
 MKAnnotationView *view = [_map dequeueReusableAnnotationViewWithIdentifier:@"CopAnnotation"];
 
 if (view == nil) {
 view = cop.annotationView;
 } else {
 view.annotation = annotation;
 }
 
 return view;
 } else {
 return nil;
 }
 }
 */

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    AppDelegate *app = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    CLLocation *newLocation;
    
    for(int i=0;i<locations.count;i++){
        newLocation = [locations objectAtIndex:i];
        break;
    }

    [_map removeOverlays:_map.overlays];
    
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:newLocation.coordinate radius:500];
    [_map addOverlay:circle];
    
    // get coords
    CLLocationCoordinate2D location = [newLocation coordinate];
    
    // Zoom Region
    MKCoordinateRegion zoomRegion = MKCoordinateRegionMakeWithDistance(location, 2500, 2500);
    
    // Show location
    [self.map setRegion:zoomRegion animated:YES];

    if (app.lastScanLocation == nil) {
        app.lastScanLocation = newLocation;
    } else {
        CLLocationDistance meters = [app.lastScanLocation distanceFromLocation:newLocation];
        if (meters > 10) {
            [self checkForCops];
            app.lastScanLocation = newLocation;
        }
    }
    
    app.currentLocation = newLocation;
}

- (void) checkForCops {
    AppDelegate *app = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    bool copFound = false;
    
    // check for cops in radius
    for (int i = 0; i < [app.cops count]; i++) {
        Cop *cop = app.cops[i];
        CLLocation * copLocation = [[CLLocation alloc] initWithLatitude:[cop.latitude doubleValue] longitude: [cop.longitude doubleValue]];
        CLLocationDistance meters = [copLocation distanceFromLocation:app.currentLocation];
        
        if (meters <= app.copRadius) {
            copFound = true;
            break;
        }
    }
    
    if (copFound) {
        if ( ! app.inCopZone) {
            [self copAlert];
            app.inCopZone = true;
        }
    } else {
        app.inCopZone = false;
    }
}

- (void) copAlert {
    // send local notification, also play sound
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
    localNotification.alertBody = @"Watch out for cops!";
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (!([defaults boolForKey:@"soundOff"])) {
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
        //NSString *path = [NSString stringWithFormat:@"%@/siren.mp3", [[NSBundle mainBundle] resourcePath]];
        //NSURL *fileURL = [NSURL fileURLWithPath:path];
        
        NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"siren" ofType:@"mp3"];
        NSURL *soundUrl = [NSURL fileURLWithPath:soundPath];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundUrl, &self->_soundID);
        AudioServicesPlaySystemSound(self->_soundID);
        
        //AVAudioPlayer *player = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error: nil];
        //[player play];
    }
    
}

- (MKOverlayView *)mapView:(MKMapView *)map viewForOverlay:(id <MKOverlay>)overlay
{
    MKCircleView *circleView = [[MKCircleView alloc] initWithOverlay:overlay];
    circleView.strokeColor = [UIColor redColor];
    circleView.fillColor = [[UIColor redColor] colorWithAlphaComponent:0.4];
    return circleView;
}
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    
    return toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    static NSString *AnnotationViewID = @"annotationViewID";
    
    MKAnnotationView *annotationView = (MKAnnotationView *)[_map dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    
    if (annotationView == nil)
    {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
    }
    annotationView.image = [UIImage imageNamed:@"copcaricon.png"];//add any image which you want to show on map instead of red pins
    annotationView.annotation = annotation;
    
    return annotationView;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
