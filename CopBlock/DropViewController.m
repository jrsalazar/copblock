//
//  DropViewController.m
//  CopBlock
//
//  Created by Rory Trankler on 10/16/14.
//  Copyright (c) 2014 Coolio Software. All rights reserved.
//

#import "DropViewController.h"
#import "AppDelegate.h"

@interface DropViewController ()

@end

@implementation DropViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Define an action for the button
    [self.btnDrop addTarget:self action:@selector(btnClicked) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)btnClicked {
    // add current location to list
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    
    NSString *url = @"http://wise-chalice-771.appspot.com/addCop.php?lat=";
    url = [url stringByAppendingString:[NSString stringWithFormat:@"%f", [app currentLocation].coordinate.latitude]];
    url = [url stringByAppendingString:@"&long="];
    url = [url stringByAppendingString:[NSString stringWithFormat:@"%f", [app currentLocation].coordinate.longitude]];
    
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
