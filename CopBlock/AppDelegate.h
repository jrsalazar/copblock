//
//  AppDelegate.h
//  CopBlock
//
//  Created by Charles Trankler on 10/16/14.
//  Copyright (c) 2014 Coolio Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (strong, nonatomic) CLLocation *lastScanLocation;
@property (nonatomic, strong) NSMutableArray *cops;
@property bool inCopZone;
@property int copRadius;
@property int newestCopId;
@property int copDays;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
