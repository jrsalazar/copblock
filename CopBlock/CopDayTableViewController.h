//
//  CopDayTableViewController.h
//  CopBlock
//
//  Created by Justin Salazar on 12/1/14.
//  Copyright (c) 2014 Coolio Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CopDayTableViewController : UITableViewController

@property int selectedRow;

@end
