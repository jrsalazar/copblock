//
//  DropViewController.h
//  CopBlock
//
//  Created by Rory Trankler on 10/16/14.
//  Copyright (c) 2014 Coolio Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnDrop;

@end