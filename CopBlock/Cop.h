//
//  Cop.h
//  CopBlock
//
//  Created by Rory Trankler on 11/22/14.
//  Copyright (c) 2014 Coolio Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cop : NSObject

@property (strong, nonatomic) NSNumber *copId;
@property (strong, nonatomic) NSNumber *latitude;
@property (strong, nonatomic) NSNumber *longitude;
@property (strong, nonatomic) NSString *createDate;

@end
