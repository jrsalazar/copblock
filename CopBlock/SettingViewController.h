//
//  SettingViewController.h
//  CopBlock
//
//  Created by Justin Salazar on 11/22/14.
//  Copyright (c) 2014 Coolio Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface SettingViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *segControl;
- (IBAction)segChanged:(UISegmentedControl *)sender;
@property (weak, nonatomic) IBOutlet UISwitch *remUnlocked;
- (IBAction)changeUnlocked:(UISwitch *)sender;
@property (weak, nonatomic) IBOutlet UISwitch *soundSwitch;
- (IBAction)soundSwitchChanged:(UISwitch *)sender;

@end
